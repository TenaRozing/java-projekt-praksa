import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;


class Server {
	public static void main(String[] args) throws IOException {
		final int PORT = 4040;
		ServerSocket serverSocket = new ServerSocket(PORT);

		System.out.println("Server started");
		System.out.println("Waiting for clients.");

		while (true) {
			Socket clientSocket = serverSocket.accept();
			Thread t = new Thread() {
				public void run() {
					try (PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true); Scanner in = new Scanner(clientSocket.getInputStream());) {
						while (in.hasNextLine()) {
							String input = in.nextLine();
							if (input.equalsIgnoreCase("exit")) {
								break;
							}
							System.out.println("Received string from client: " + input);

							if (input.equalsIgnoreCase("ping")) {
								out.println("pong");
							} else {
								out.println("Invalid input");
							}
						}
					} catch (IOException e) {
					}
				}
			};
			t.start();
		}
	}
}